package eu.eudat.file.transformer.controller;

import eu.eudat.file.transformer.interfaces.FileTransformerClient;
import eu.eudat.file.transformer.interfaces.FileTransformerConfiguration;
import eu.eudat.file.transformer.models.description.DescriptionFileTransformerModel;
import eu.eudat.file.transformer.models.description.DescriptionXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpXmlModel;
import eu.eudat.file.transformer.models.dmpblueprint.DmpBlueprintFileTransformerModel;
import eu.eudat.file.transformer.models.misc.FileEnvelope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/file")
public class FileTransformerController {

    private final FileTransformerClient fileTransformerExecutor;

    @Autowired
    public FileTransformerController(FileTransformerClient fileTransformerExecutor) {
        this.fileTransformerExecutor = fileTransformerExecutor;
    }

    @PostMapping("/export/dmp")
    public FileEnvelope exportDmp(@RequestBody DmpFileTransformerModel dmpDepositModel) throws Exception {
        return fileTransformerExecutor.exportDmp(dmpDepositModel);
    }

    @PostMapping("/export/description")
    public FileEnvelope exportDescription(@RequestBody DescriptionFileTransformerModel descriptionXmlModel, @RequestParam(value = "format",required = false)String format, @RequestParam(value = "descriptionId",required = false) String descriptionId) throws Exception {
        return fileTransformerExecutor.exportDescription(descriptionXmlModel, format);
    }

    @PostMapping("/import/dmp")
    public DmpFileTransformerModel importFileToDmp(@RequestBody FileEnvelope fileEnvelope) {
        return fileTransformerExecutor.importDmp(fileEnvelope);
    }

    @PostMapping("/import/description")
    public DescriptionFileTransformerModel importFileToDescription(@RequestBody FileEnvelope fileEnvelope) {
        return fileTransformerExecutor.importDescription(fileEnvelope);
    }

    @GetMapping("/formats")
    public FileTransformerConfiguration getSupportedFormats() {
        return fileTransformerExecutor.getConfiguration();
    }
}
