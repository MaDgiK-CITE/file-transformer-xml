# Using XML File Transformer with Argos

The repository-file-transformer-xml module implements the [https://code-repo.d4science.org/MaDgiK-CITE/file-transformer-base](https://) interface for the Xml file format.

## Setup

After creating the jar from the project, environment variables should be set since they are used in the application.properties
1) STORAGE_TMP_ZENODO - a temporary storage needed
2) CONFIGURATION_ZENODO - path to json file which includes the configuration for the repository