package eu.eudat.file.transformer.models.reference;


import eu.eudat.file.transformer.enums.ReferenceSourceType;
import eu.eudat.file.transformer.enums.ReferenceType;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.UUID;

@XmlRootElement(name = "Reference")
public class ReferenceXmlModel {

	private UUID id;
	private String label;
	private ReferenceType type;
	private String description;
	private DefinitionXmlModel definition;
	private String reference;
	private String abbreviation;
	private String source;
	private ReferenceSourceType sourceType;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ReferenceType getType() {
		return type;
	}

	public void setType(ReferenceType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DefinitionXmlModel getDefinition() {
		return definition;
	}

	public void setDefinition(DefinitionXmlModel definition) {
		this.definition = definition;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public ReferenceSourceType getSourceType() {
		return sourceType;
	}

	public void setSourceType(ReferenceSourceType sourceType) {
		this.sourceType = sourceType;
	}
}
