package eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata;

public class LabelAndMultiplicityDataXmlModel extends BaseFieldDataXmlModel {
    private Boolean multipleSelect;

    public Boolean getMultipleSelect() {
        return multipleSelect;
    }

    public void setMultipleSelect(Boolean multipleSelect) {
        this.multipleSelect = multipleSelect;
    }
}
