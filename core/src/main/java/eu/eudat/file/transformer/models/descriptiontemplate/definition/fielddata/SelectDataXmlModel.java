package eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata;

import java.util.List;

public class SelectDataXmlModel extends LabelAndMultiplicityDataXmlModel {
    private List<OptionXmlModel> options;

    public List<OptionXmlModel> getOptions() {
        return options;
    }

    public void setOptions(List<OptionXmlModel> optionEntities) {
        this.options = optionEntities;
    }

    public static class OptionXmlModel {
        private String label;
        private String value;

        public String getLabel() {
            return label;
        }
        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }
    }
}

