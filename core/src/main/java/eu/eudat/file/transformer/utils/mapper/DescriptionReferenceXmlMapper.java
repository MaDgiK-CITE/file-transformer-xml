package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.description.DescriptionReferenceFileTransformerModel;
import eu.eudat.file.transformer.models.description.DescriptionReferenceXmlModel;
import eu.eudat.file.transformer.utils.mapper.reference.ReferenceXmlMapper;

public class DescriptionReferenceXmlMapper {

    public static DescriptionReferenceXmlModel toXml(DescriptionReferenceFileTransformerModel fileModel) {
        DescriptionReferenceXmlModel xmlModel = new DescriptionReferenceXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setReference(ReferenceXmlMapper.toXml(fileModel.getReference()));
        return xmlModel;
    }
}
