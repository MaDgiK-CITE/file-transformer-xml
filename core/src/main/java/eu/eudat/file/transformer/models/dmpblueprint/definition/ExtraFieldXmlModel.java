package eu.eudat.file.transformer.models.dmpblueprint.definition;

import eu.eudat.file.transformer.enums.DmpBlueprintExtraFieldDataType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ExtraField")
public class ExtraFieldXmlModel extends FieldXmlModel {

	private DmpBlueprintExtraFieldDataType dataType;

	public DmpBlueprintExtraFieldDataType getDataType() {
		return dataType;
	}

	public void setDataType(DmpBlueprintExtraFieldDataType dataType) {
		this.dataType = dataType;
	}
}
