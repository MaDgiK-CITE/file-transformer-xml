package eu.eudat.file.transformer.models.descriptiontemplate.definition;

import jakarta.xml.bind.annotation.XmlType;

import java.util.List;

@XmlType(namespace = "description-template")
public class DefinitionXmlModel {

	private List<PageXmlModel> pages;

	public List<PageXmlModel> getPages() {
		return pages;
	}

	public void setPages(List<PageXmlModel> pages) {
		this.pages = pages;
	}
}
