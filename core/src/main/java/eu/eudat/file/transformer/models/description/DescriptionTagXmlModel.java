package eu.eudat.file.transformer.models.description;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.eudat.file.transformer.models.tag.TagXmlModel;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescriptionTagXmlModel {

    private UUID id;
    private TagXmlModel tagXmlModel;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public TagXmlModel getTagFileTransformerModel() {
        return tagXmlModel;
    }

    public void setTagFileTransformerModel(TagXmlModel tagXmlModel) {
        this.tagXmlModel = tagXmlModel;
    }
}
