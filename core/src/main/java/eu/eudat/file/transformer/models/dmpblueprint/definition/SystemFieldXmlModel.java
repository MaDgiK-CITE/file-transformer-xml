package eu.eudat.file.transformer.models.dmpblueprint.definition;

import eu.eudat.file.transformer.enums.DmpBlueprintSystemFieldType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SystemField")
public class SystemFieldXmlModel extends FieldXmlModel {

	private DmpBlueprintSystemFieldType systemFieldType;

	public DmpBlueprintSystemFieldType getSystemFieldType() {
		return systemFieldType;
	}

	public void setSystemFieldType(DmpBlueprintSystemFieldType systemFieldType) {
		this.systemFieldType = systemFieldType;
	}
}
