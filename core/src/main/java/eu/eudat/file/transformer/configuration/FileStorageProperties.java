package eu.eudat.file.transformer.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;

@ConfigurationProperties(prefix = "file.storage")
public class FileStorageProperties {
    private final String temp;
    private final String transientPath;

    @ConstructorBinding
    public FileStorageProperties(String temp, String transientPath) {
        this.temp = temp;
        this.transientPath = transientPath;
    }

    public String getTemp() {
        return temp;
    }

    public String getTransientPath() {
        return transientPath;
    }
}
