package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.description.DescriptionTagFileTransformerModel;
import eu.eudat.file.transformer.models.description.DescriptionTagXmlModel;
import eu.eudat.file.transformer.models.tag.TagFileTransformerModel;
import eu.eudat.file.transformer.models.tag.TagXmlModel;

public class DescriptionTagXmlMapper {

    public static DescriptionTagXmlModel toXml(DescriptionTagFileTransformerModel fileModel) {
        DescriptionTagXmlModel xmlModel = new DescriptionTagXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setTagFileTransformerModel(toTagXml(fileModel.getTagFileTransformerModel()));
        return xmlModel;
    }

    private static TagXmlModel toTagXml(TagFileTransformerModel fileModel) {
        TagXmlModel xmlModel = new TagXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setLabel(fileModel.getLabel());
        return xmlModel;
    }
}
