package eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata;


import eu.eudat.file.transformer.enums.FieldDataExternalDatasetType;
import eu.eudat.file.transformer.enums.FieldType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = FieldType.Names.ExternalDatasets)
public class ExternalDatasetDataXmlModel extends LabelAndMultiplicityDataXmlModel {
    private FieldDataExternalDatasetType type;

    public FieldDataExternalDatasetType getType() {
        return type;
    }

    public void setType(FieldDataExternalDatasetType type) {
        this.type = type;
    }
}
