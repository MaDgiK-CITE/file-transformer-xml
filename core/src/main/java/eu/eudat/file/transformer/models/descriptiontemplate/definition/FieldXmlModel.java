package eu.eudat.file.transformer.models.descriptiontemplate.definition;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import eu.eudat.file.transformer.enums.FieldType;
import eu.eudat.file.transformer.enums.FieldValidationType;
import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata.*;
import jakarta.xml.bind.annotation.*;

import java.util.List;

@XmlType(namespace = "description-template")
@XmlAccessorType(XmlAccessType.FIELD)
public class FieldXmlModel {

	private String id;
	private Integer ordinal;
	private String numbering; //TODO maybe remove
	private List<String> schematics;
	private String defaultValue;
	private List<RuleXmlModel> visibilityRules;
	private List<FieldValidationType> validations;
	private Boolean includeInExport;

	@XmlElements({
			@XmlElement(type = ExternalSelectDataXmlModel.class, name = FieldType.Names.ExternalSelect),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.BooleanDecision),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.InternalDmpDatasets),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.InternalDmpDmps),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.CheckBox),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.DatePicker),
			@XmlElement(type = ExternalDatasetDataFileTransformerModel.class, name = FieldType.Names.ExternalDatasets),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.FreeText),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Licenses),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Organizations),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Publications),
			@XmlElement(type = RadioBoxDataFileTransformerModel.class, name = FieldType.Names.RadioBox),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Registries),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.InternalDmpResearchers),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Researchers),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.RichTextarea),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Services),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.Tags),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Taxonomies),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.TextArea),
			@XmlElement(type = UploadDataFileTransformerModel.class, name = FieldType.Names.Upload),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.Validation),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.DatasetIdentifier),
			@XmlElement(type = LabelDataFileTransformerModel.class, name = FieldType.Names.Currency),
			@XmlElement(type = SelectDataFileTransformerModel.class, name = FieldType.Names.Select),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.DataRepositories),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.JournalRepositories),
			@XmlElement(type = LabelAndMultiplicityDataFileTransformerModel.class, name = FieldType.Names.Publications),
	})
	private BaseFieldDataXmlModel data;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public String getNumbering() {
		return numbering;
	}

	public void setNumbering(String numbering) {
		this.numbering = numbering;
	}

	public List<String> getSchematics() {
		return schematics;
	}

	public void setSchematics(List<String> schematics) {
		this.schematics = schematics;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public List<RuleXmlModel> getVisibilityRules() {
		return visibilityRules;
	}

	public void setVisibilityRules(List<RuleXmlModel> visibilityRuleXmlModels) {
		this.visibilityRules = visibilityRuleXmlModels;
	}

	public List<FieldValidationType> getValidations() {
		return validations;
	}

	public void setValidations(List<FieldValidationType> validations) {
		this.validations = validations;
	}

	public Boolean getIncludeInExport() {
		return includeInExport;
	}

	public void setIncludeInExport(Boolean includeInExport) {
		this.includeInExport = includeInExport;
	}

	public BaseFieldDataXmlModel getData() {
		return data;
	}

	public void setData(BaseFieldDataXmlModel data) {
		this.data = data;
	}
}
