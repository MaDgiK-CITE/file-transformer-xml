package eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata;

import eu.eudat.file.transformer.enums.FieldType;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;
@XmlRootElement(name = FieldType.Names.RadioBox)
public class RadioBoxDataXmlModel extends BaseFieldDataXmlModel {
    public static class RadioBoxDataOptionXmlModel {
        private String label;
        private String value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private List<RadioBoxDataOptionXmlModel> options;

    public List<RadioBoxDataOptionXmlModel> getOptions() {
        return options;
    }

    public void setOptions(List<RadioBoxDataOptionXmlModel> options) {
        this.options = options;
    }
}
