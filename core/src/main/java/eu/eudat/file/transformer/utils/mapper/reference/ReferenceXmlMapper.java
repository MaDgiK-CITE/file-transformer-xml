package eu.eudat.file.transformer.utils.mapper.reference;

import eu.eudat.file.transformer.models.reference.ReferenceFileTransformerModel;
import eu.eudat.file.transformer.models.reference.ReferenceXmlModel;

public class ReferenceXmlMapper {

    public static ReferenceXmlModel toXml(ReferenceFileTransformerModel fileModel) {
        ReferenceXmlModel xmlModel = new ReferenceXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setDescription(fileModel.getDescription());
        xmlModel.setReference(fileModel.getReference());
        xmlModel.setAbbreviation(fileModel.getAbbreviation());
        xmlModel.setSource(fileModel.getSource());
        xmlModel.setType(fileModel.getType());
        xmlModel.setSourceType(fileModel.getSourceType());
        if (fileModel.getDefinition() != null) {
            xmlModel.setDefinition(DefinitionXmlMapper.toXml(fileModel.getDefinition()));
        }
        return xmlModel;
    }
}
