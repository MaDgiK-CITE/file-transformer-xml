package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.enums.DmpBlueprintFieldCategory;
import eu.eudat.file.transformer.models.dmpblueprint.DmpBlueprintFileTransformerModel;
import eu.eudat.file.transformer.models.dmpblueprint.DmpBlueprintXmlModel;
import eu.eudat.file.transformer.models.dmpblueprint.definition.*;

public class DmpBlueprintXmlMapper {

    public static DmpBlueprintXmlModel toXml(DmpBlueprintFileTransformerModel fileModel) {
        DmpBlueprintXmlModel xmlModel = new DmpBlueprintXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setDefinitionFileTransformerModel(toDefinitionXml(fileModel.getDefinitionFileTransformerModel()));
        return xmlModel;
    }

    private static DefinitionXmlModel toDefinitionXml(DefinitionFileTransformerModel fileModel) {
        DefinitionXmlModel xmlModel = new DefinitionXmlModel();
        xmlModel.setSections(fileModel.getSections().stream().map(DmpBlueprintXmlMapper::toSectionXml).toList());
        return xmlModel;
    }

    private static SectionXmlModel toSectionXml(SectionFileTransformerModel fileModel) {
        SectionXmlModel xmlModel = new SectionXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setDescription(fileModel.getDescription());
        xmlModel.setHasTemplates(fileModel.getHasTemplates());
        xmlModel.setOrdinal(fileModel.getOrdinal());
        if (fileModel.getFields() != null) {
            xmlModel.setFields(fileModel.getFields().stream().map(DmpBlueprintXmlMapper::toFieldXml).toList());
        }
        return xmlModel;
    }

    private static FieldXmlModel toFieldXml(FieldFileTransformerModel fileModel) {
        FieldXmlModel xmlModel = switch (fileModel) {
            case SystemFieldFileTransformerModel sm -> new SystemFieldXmlModel();
            case ExtraFieldFileTransformerModelFileTransformerModel sm -> new ExtraFieldXmlModel();
            default -> null;
        };

        if (xmlModel != null) {
            xmlModel.setId(fileModel.getId());
            xmlModel.setCategory(fileModel.getCategory());
            xmlModel.setPlaceholder(fileModel.getPlaceholder());
            xmlModel.setDescription(fileModel.getDescription());
            xmlModel.setLabel(fileModel.getLabel());
            xmlModel.setOrdinal(fileModel.getOrdinal());
        }

        return xmlModel;
    }
}
