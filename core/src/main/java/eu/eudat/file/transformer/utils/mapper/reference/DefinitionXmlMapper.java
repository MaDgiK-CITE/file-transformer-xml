package eu.eudat.file.transformer.utils.mapper.reference;

import eu.eudat.file.transformer.models.reference.DefinitionFileTransformerModel;
import eu.eudat.file.transformer.models.reference.DefinitionXmlModel;
import eu.eudat.file.transformer.models.reference.FieldFileTransformerModel;
import eu.eudat.file.transformer.models.reference.FieldXmlModel;

public class DefinitionXmlMapper {

    public static DefinitionXmlModel toXml(DefinitionFileTransformerModel fileModel) {
        DefinitionXmlModel xmlModel = new DefinitionXmlModel();
        xmlModel.setFields(fileModel.getFields().stream().map(DefinitionXmlMapper::toFieldXml).toList());
        return xmlModel;
    }

    private static FieldXmlModel toFieldXml(FieldFileTransformerModel fileModel) {
        FieldXmlModel xmlModel = new FieldXmlModel();
        xmlModel.setCode(fileModel.getCode());
        xmlModel.setValue(fileModel.getValue());
        return xmlModel;
    }
}
