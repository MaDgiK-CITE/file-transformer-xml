package eu.eudat.file.transformer.executor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import eu.eudat.file.transformer.configuration.FileStorageProperties;
import eu.eudat.file.transformer.enums.ReferenceType;
import eu.eudat.file.transformer.interfaces.FileTransformerClient;
import eu.eudat.file.transformer.interfaces.FileTransformerConfiguration;
import eu.eudat.file.transformer.models.description.DescriptionFileTransformerModel;
import eu.eudat.file.transformer.models.description.DescriptionXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpReferenceFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpXmlModel;
import eu.eudat.file.transformer.models.dmpblueprint.DmpBlueprintFileTransformerModel;
import eu.eudat.file.transformer.models.misc.FileEnvelope;
import eu.eudat.file.transformer.models.misc.FileFormat;
import eu.eudat.file.transformer.models.reference.ReferenceFileTransformerModel;
import eu.eudat.file.transformer.utils.mapper.DMPXmlMapper;
import eu.eudat.file.transformer.utils.mapper.DescriptionXmlMapper;
import eu.eudat.file.transformer.utils.service.storage.FileStorageService;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.InvalidApplicationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class XmlFileTransformer implements FileTransformerClient {
    private final static Logger logger = LoggerFactory.getLogger(XmlFileTransformer.class);
    private final ObjectMapper mapper;
    private final FileStorageService fileStorageService;
    private final FileStorageProperties fileStorageProperties;

    @Autowired
    public XmlFileTransformer(FileStorageService fileStorageService, FileStorageProperties fileStorageProperties) {
        this.fileStorageService = fileStorageService;
        this.fileStorageProperties = fileStorageProperties;
        mapper = new XmlMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }
    @Override
    public FileEnvelope exportDmp(DmpFileTransformerModel dmpXmlModel) throws InvalidApplicationException, IOException {

        List<ReferenceFileTransformerModel> grants = new ArrayList<>();
        if (dmpXmlModel.getDmpReferences() != null) {
            grants = dmpXmlModel.getDmpReferences().stream().map(DmpReferenceFileTransformerModel::getReference).filter(referenceFileModel -> referenceFileModel.getType().equals(ReferenceType.Grants)).toList();
        }

        try {
            DmpXmlModel xmlModel = DMPXmlMapper.toXml(dmpXmlModel);
            JAXBContext content = JAXBContext.newInstance(DmpXmlModel.class);
            Marshaller marshaller = content.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            ByteArrayOutputStream xmlDoc = new ByteArrayOutputStream();
            marshaller.marshal(xmlModel, xmlDoc);
            String fileRef = fileStorageService.storeFile(xmlDoc.toByteArray());
            FileEnvelope fileEnvelope = new FileEnvelope();
            fileEnvelope.setFile(fileRef);
            //TODO
            if (!grants.isEmpty() && grants.get(0) != null && grants.get(0).getLabel() != null) {
                fileEnvelope.setFilename("DMP_" + grants.get(0).getLabel() + "_" + dmpXmlModel.getVersion() + ".xml");
            } else {
                fileEnvelope.setFilename("DMP_" + dmpXmlModel.getLabel() + "_" + dmpXmlModel.getVersion() + ".xml");
            }
            return fileEnvelope;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    @Override
    public FileEnvelope exportDescription(DescriptionFileTransformerModel descriptionXmlModel, String format) throws InvalidApplicationException, IOException {
        try {
            DescriptionXmlModel xmlModel = DescriptionXmlMapper.toXml(descriptionXmlModel);
            JAXBContext content = JAXBContext.newInstance(DmpXmlModel.class);
            Marshaller marshaller = content.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            ByteArrayOutputStream xmlDoc = new ByteArrayOutputStream();
            marshaller.marshal(xmlModel, xmlDoc);
            String fileRef = fileStorageService.storeFile(xmlDoc.toByteArray());
            FileEnvelope fileEnvelope = new FileEnvelope();
            fileEnvelope.setFile(fileRef);
            //TODO
       /* if (!grants.isEmpty() && grants.get(0) != null && grants.get(0).getLabel() != null) {
            fileEnvelope.setFilename("DMP_" + grants.get(0).getLabel() + "_" + dmpFileTransformerModel.getVersion() + ".xml");
        }
        else */
            {
                fileEnvelope.setFilename("Description_" + descriptionXmlModel.getLabel() + "_" + descriptionXmlModel.getDmp().getVersion() + ".xml");
            }
            return fileEnvelope;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    @Override
    public DmpFileTransformerModel importDmp(FileEnvelope fileEnvelope) {
        /*try { //TODO
            String jsonString = String.valueOf(this.fileStorageService.readFile(fileEnvelope.getFile()));
            RDAModel rda = mapper.readValue(jsonString, RDAModel.class);
            DmpFileTransformerModel model = this.dmpRDAMapper.toEntity(rda.getDmp(), )
        } catch (JsonProcessingException e) {

        }*/
        return null;
    }

    @Override
    public DescriptionFileTransformerModel importDescription(FileEnvelope fileEnvelope) {
        return null;
    }

    @Override
    public FileTransformerConfiguration getConfiguration() {
        List<FileFormat> supportedFormats = List.of(new FileFormat("xml", true, "fa-file-code-o"));
        FileTransformerConfiguration configuration = new FileTransformerConfiguration();
        configuration.setFileTransformerId("xml");
        configuration.setExportVariants(supportedFormats);
        configuration.setImportVariants(supportedFormats);
        return configuration;
    }
}
