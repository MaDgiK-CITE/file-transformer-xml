package eu.eudat.file.transformer.models.dmp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.eudat.file.transformer.models.reference.ReferenceXmlModel;

import java.util.UUID;

public class DmpReferenceXmlModel {

    private UUID id;
    private ReferenceXmlModel reference;
    private String data;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ReferenceXmlModel getReference() {
        return reference;
    }

    public void setReference(ReferenceXmlModel reference) {
        this.reference = reference;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
