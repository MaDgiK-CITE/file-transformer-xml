package eu.eudat.file.transformer.xml.datasetProfileXml;

import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.FieldXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.FieldSetXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.PageXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.SectionXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata.*;
import eu.eudat.file.transformer.xml.XmlBuilder;
import org.springframework.core.env.Environment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


public class ExportXmlBuilderDatasetProfile {


    public File build(DescriptionTemplateXmlModel datasetProfile, Environment environment) throws IOException {

        File xmlFile = new File(environment.getProperty("temp.temp") + UUID.randomUUID() + ".xml");
        BufferedWriter writer = new BufferedWriter(new FileWriter(xmlFile, true));
        Document xmlDoc = XmlBuilder.getDocument();
//        Element root = xmlDoc.createElement("root");
//        root.appendChild(createPages(datasetProfile.getPages(), datasetProfile.getSections(), xmlDoc));

        xmlDoc.appendChild(createPages(datasetProfile.getDefinition().getPages(), xmlDoc));
        Element pages = (Element)xmlDoc.getFirstChild();
        pages.setAttribute("description", datasetProfile.getDescription());
        pages.setAttribute("language", datasetProfile.getLanguage());
        pages.setAttribute("type", datasetProfile.getType().getName());
        String xml = XmlBuilder.generateXml(xmlDoc);
        writer.write(xml);
        writer.close();
        return xmlFile;
    }

    public Element createPages(List<PageXmlModel> datasetProfilePages, Document element) {
        Element pages = element.createElement("pages");
        datasetProfilePages.forEach(item -> {
            Element page = element.createElement("page");
            page.setAttribute("id", "" + item.getId());
            page.setAttribute("ordinal", "" + item.getOrdinal());
            page.setAttribute("title", "" + item.getTitle());
            Element elementSections = element.createElement("sections");
            page.appendChild(createSections(item.getSections(), element, elementSections));

            pages.appendChild(page);
        });
        return pages;
    }


    public Element createSections(List<SectionXmlModel> sections, Document element, Element elementSections) {
//        if (sections.getSections() != null) {
        for (SectionXmlModel section : sections) {
            if (section.getSections() != null && !section.getSections().isEmpty()) {
                Element elementSectionsChild = element.createElement("section");
                elementSections.appendChild(createSections(section.getSections(), element, elementSectionsChild));

            }
//        }

            elementSections.setAttribute("defaultVisibility", "" + section.getDefaultVisibility());
            elementSections.setAttribute("id", "" + section.getId());
            elementSections.setAttribute("ordinal", "" + section.getOrdinal());

            if (section.getFieldSets() != null && !section.getFieldSets().isEmpty()) {
                elementSections.appendChild(createFieldSet(section.getFieldSets(), element));
            }

            if (section.getNumbering() != null) {
                Element numbering = element.createElement("numbering");
                numbering.setTextContent(section.getNumbering());
                elementSections.appendChild(numbering);
            }
            if (section.getDescription() != null) {
                Element description = element.createElement("description");
                description.setTextContent(section.getDescription());
                elementSections.appendChild(description);
            }
            if (section.getTitle() != null) {
                Element title = element.createElement("title");
                title.setTextContent(section.getTitle());
                elementSections.appendChild(title);
            }
        }

        return elementSections;
    }

    public Element createFieldSet(List<FieldSetXmlModel> fieldSet, Document element) {
        Element elementFieldSets = element.createElement("field-Sets");
        fieldSet.forEach(field -> {
            Element composite = element.createElement("field-Set");
            composite.setAttribute("id", field.getId());
            composite.setAttribute("ordinal", "" + field.getOrdinal());

            if (field.getNumbering() != null) {
                Element numbering = element.createElement("numbering");
                numbering.setTextContent(field.getNumbering());
                composite.appendChild(numbering);
            }
            Element commentField = element.createElement("CommentField");
            commentField.setTextContent("" + field.getHasCommentField());
            composite.appendChild(commentField);

            composite.appendChild(createFields(field.getFields(), element));

            if (field.getMultiplicity() != null) {
                Element multiplicity = element.createElement("multiplicity");
                multiplicity.setAttribute("max", "" + field.getMultiplicity().getMax());
                multiplicity.setAttribute("min", "" + field.getMultiplicity().getMin());
                multiplicity.setAttribute("placeholder", field.getMultiplicity().getPlaceholder());
                multiplicity.setAttribute("tableView", String.valueOf(field.getMultiplicity().getTableView()));
                composite.appendChild(multiplicity);
            }
            if (field.getTitle() != null && !field.getTitle().isEmpty()) {
                Element title = element.createElement("title");
                title.setTextContent(field.getTitle());
                composite.appendChild(title);
            }
            if (field.getDescription() != null && !field.getDescription().isEmpty()) {
                Element title = element.createElement("description");
                title.setTextContent(field.getDescription());
                composite.appendChild(title);
            }
            if (field.getExtendedDescription() != null && !field.getExtendedDescription().isEmpty()) {
                Element extendedDescription = element.createElement("extendedDescription");
                extendedDescription.setTextContent(field.getExtendedDescription());
                composite.appendChild(extendedDescription);
            }
            if (field.getAdditionalInformation() != null && !field.getAdditionalInformation().isEmpty()) {
                Element additionalInformation = element.createElement("additionalInformation");
                additionalInformation.setTextContent(field.getAdditionalInformation());
                composite.appendChild(additionalInformation);
            }


            elementFieldSets.appendChild(composite);
        });
        return elementFieldSets;
    }


    public Element createFields(List<FieldXmlModel> fields, Document element) {
        Element elementFields = element.createElement("fields");
        fields.forEach(field -> {
            Element elementField = element.createElement("field");
            elementField.setAttribute("id", field.getId());
            elementField.setAttribute("ordinal", "" + field.getOrdinal());

            if (field.getNumbering() != null) {
                Element numbering = element.createElement("numbering");
                numbering.setTextContent(field.getNumbering());
                elementField.appendChild(numbering);
            }
            if (field.getSchematics() != null) {
                Element schematics = element.createElement("schematics");
                field.getSchematics().forEach(schematic -> {
                    Element schematicChild = element.createElement("schematic");
                    schematicChild.setTextContent(schematic);
                    schematics.appendChild(schematicChild);
                });
                elementField.appendChild(schematics);
            }
            if (field.getValidations() != null) {
                Element validations = element.createElement("validations");
                field.getValidations().forEach(validation -> {
                    Element validationChild = element.createElement("validation");
                    validationChild.setAttribute("type", "" + validation);
                    validations.appendChild(validationChild);
                });
                elementField.appendChild(validations);
            }
            if (field.getDefaultValue() != null) {
                Element defaultValue = element.createElement("defaultValue");
                defaultValue.setAttribute("type", field.getData().getFieldType().getValue());
                defaultValue.setAttribute("value", field.getData().getValue());
                elementField.appendChild(defaultValue);
            }
            if (field.getVisibilityRules() != null) {
                Element visible = element.createElement("visible");
                //visible.setAttribute("style", "" + field.getData()..getStyle());
                field.getVisibilityRules().forEach(rule -> {
                    Element ruleChild = element.createElement("rule");
                    ruleChild.setAttribute("target", "" + rule.getTarget());
                    Element ruleChildValue = element.createElement("value");
                    ruleChildValue.setTextContent(rule.getValue());
                    ruleChild.appendChild(ruleChildValue);
                    visible.appendChild(ruleChild);

                });
                elementField.appendChild(visible);
            }
           /* if (field.getViewStyle() != null) {
                Element viewStyle = element.createElement("viewStyle");
                viewStyle.setAttribute("cssClass", field.getViewStyle().getCssClass());
                viewStyle.setAttribute("renderStyle", field.getViewStyle().getFieldType().getValue());
                elementField.appendChild(viewStyle);
            }*/

            if (field.getData() != null) {
                Element dataOut = element.createElement("data");
                switch (field.getData().getFieldType()) {
                    case SELECT:
                        {
                            SelectDataXmlModel wordListDataEntityObject = (SelectDataXmlModel) field.getData();
                            dataOut.setAttribute("label", wordListDataEntityObject.getLabel());
                            dataOut.setAttribute("type", wordListDataEntityObject.getFieldType().getValue());
                            dataOut.setAttribute("multipleSelect", wordListDataEntityObject.getMultipleSelect().toString());
                            Element options = element.createElement("options");
                            wordListDataEntityObject.getOptions().forEach(optionChildFor -> {
                                Element optionChild = element.createElement("option");
                                optionChild.setAttribute("label", optionChildFor.getLabel());
                                optionChild.setAttribute("value", optionChildFor.getValue());
                                options.appendChild(optionChild);
                            });

                            dataOut.appendChild(options);
                            break;
                        }
                    case EXTERNAL_SELECT:
                    {
                            ExternalSelectDataXmlModel autoCompleteDataEntityObject = (ExternalSelectDataXmlModel) field.getData();
                            dataOut.setAttribute("label", autoCompleteDataEntityObject.getLabel());
                            dataOut.setAttribute("type", autoCompleteDataEntityObject.getFieldType().getValue());
                            dataOut.setAttribute("multipleSelect", autoCompleteDataEntityObject.getMultipleSelect().toString());
                            for (ExternalSelectDataXmlModel.ExternalSelectSourceXmlModel singleData: autoCompleteDataEntityObject.getSources()) {
                                Element singleItem = element.createElement("selectSource");
                                singleItem.setAttribute("method", singleData.getMethod());
                                singleItem.setAttribute("url", singleData.getUrl());
                                singleItem.setAttribute("optionsRoot", singleData.getOptionsRoot());
                                singleItem.setAttribute("label", singleData.getSourceBinding().getLabel());
                                singleItem.setAttribute("value", singleData.getSourceBinding().getValue());
                                singleItem.setAttribute("source", singleData.getSourceBinding().getSource());
                                singleItem.setAttribute("hasAuth", singleData.getHasAuth().toString());

                                if (singleData.getAuth() != null) {
                                    Element authItem = element.createElement("sourceAuth");
                                    authItem.setAttribute("method", singleData.getAuth().getMethod());
                                    authItem.setAttribute("url", singleData.getAuth().getUrl());
                                    authItem.setAttribute("path", singleData.getAuth().getPath());
                                    authItem.setAttribute("body", singleData.getAuth().getBody());
                                    authItem.setAttribute("type", singleData.getAuth().getType());
                                    singleItem.appendChild(authItem);
                                }
                                dataOut.appendChild(singleItem);
                            }
                        }
                        break;
                    case UPLOAD:
                        UploadDataXmlModel uploadEntityObject = (UploadDataXmlModel) field.getData();
                        dataOut.setAttribute("label", uploadEntityObject.getLabel());
                        dataOut.setAttribute("maxFileSizeInMB", String.valueOf(uploadEntityObject.getMaxFileSizeInMB()));
                        Element types = element.createElement("types");
                        uploadEntityObject.getTypes().forEach(type -> {
                            Element optionChild = element.createElement("option");
                            optionChild.setAttribute("label", type.getLabel());
                            optionChild.setAttribute("value", type.getValue());
                            types.appendChild(optionChild);
                        });
                        dataOut.appendChild(types);
                        break;
                    case BOOLEAN_DECISION:
                        //BooleanDes booleanDecisionDataEntityObject = (BooleanDecisionDataEntity) field.getData();
                        dataOut.setAttribute("label", field.getData().getLabel());
                        break;
                    case RADIO_BOX:
                        RadioBoxDataXmlModel radioBoxDataEntityObject = (RadioBoxDataXmlModel) field.getData();
                        dataOut.setAttribute("label", radioBoxDataEntityObject.getLabel());

                        Element options = element.createElement("options");
                        radioBoxDataEntityObject.getOptions().forEach(optionChildFor -> {
                            Element optionChild = element.createElement("option");
                            optionChild.setAttribute("label", optionChildFor.getLabel());
                            optionChild.setAttribute("value", optionChildFor.getValue());
                            options.appendChild(optionChild);
                        });
                        dataOut.appendChild(options);
                        break;
                    case CHECK_BOX:
                    case FREE_TEXT:
                    case TEXT_AREA:
                    case RICH_TEXT_AREA:
                    case DATE_PICKER:
                    case DATASET_IDENTIFIER:
                    case CURRENCY:
                    case TAGS:
                        dataOut.setAttribute("label", field.getData().getLabel());
                        break;
                    /*case INTERNAL_DMP_ENTRIES_DMPS:
                        //ExternalDatasetDataFileTransformerModel internalDmpEntitiesData = (ExternalDatasetDataFileTransformerModel) field.getData();
                        dataOut.setAttribute("label", field.getData().getLabel());
                        dataOut.setAttribute("type", field.getData().getFieldType().getValue());
                        switch (internalDmpEntitiesData.getFieldSubType()) {
                            case Researchers:
                                ResearcherAutoCompleteDataEntity researchersAutoCompleteData = (ResearcherAutoCompleteDataEntity) internalDmpEntitiesData;
                                dataOut.setAttribute("multiAutocomplete", researchersAutoCompleteData.getMultiAutoComplete().toString());
                                break;
                            case Datasets:
                                DatasetAutoCompleteDataEntity datasetAutoCompleteDataEntity = (DatasetAutoCompleteDataEntity) internalDmpEntitiesData;
                                dataOut.setAttribute("multiAutocomplete", datasetAutoCompleteDataEntity.getMultiAutoComplete().toString());
                                break;
                            case Dmps:
                                DmpAutoCompleteDataEntity dmpAutoCompleteDataEntity = (DmpAutoCompleteDataEntity) internalDmpEntitiesData;
                                dataOut.setAttribute("multiAutocomplete", dmpAutoCompleteDataEntity.getMultiAutoComplete().toString());
                                break;
                        }
                        break;*/
                    case EXTERNAL_DATASETS:
                        ExternalDatasetDataXmlModel externalDatasetDataEntity = (ExternalDatasetDataXmlModel) field.getData();
                        dataOut.setAttribute("label", externalDatasetDataEntity.getLabel());
                        dataOut.setAttribute("multipleSelect", externalDatasetDataEntity.getMultipleSelect().toString());
                        dataOut.setAttribute("type", externalDatasetDataEntity.getType().getValue());
                        break;
                    case DATA_REPOSITORIES:
                    case JOURNAL_REPOSITORIES:
                    case PUB_REPOSITORIES:
                    case TAXONOMIES:
                    case LICENSES:
                    case SERVICES:
                    case REGISTRIES:
                    case VALIDATION:
                    case RESEARCHERS:
                    case PUBLICATIONS:
                    case ORGANIZATIONS:
                        LabelAndMultiplicityDataXmlModel dataRepositoryDataEntity = (LabelAndMultiplicityDataXmlModel) field.getData();
                        dataOut.setAttribute("label", dataRepositoryDataEntity.getLabel());
                        dataOut.setAttribute("multipleSelect", dataRepositoryDataEntity.getMultipleSelect().toString());
                        break;

                }
                elementField.appendChild(dataOut);
            }

            elementFields.appendChild(elementField);
        });
        return elementFields;
    }

}
