package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.dmp.DmpReferenceFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpReferenceXmlModel;
import eu.eudat.file.transformer.utils.mapper.reference.ReferenceXmlMapper;

public class DmpReferenceXmlMapper {

    public static DmpReferenceXmlModel toXml(DmpReferenceFileTransformerModel fileModel) {
        DmpReferenceXmlModel xmlModel = new DmpReferenceXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setData(fileModel.getData());
        xmlModel.setReference(ReferenceXmlMapper.toXml(fileModel.getReference()));
        return xmlModel;
    }
}
