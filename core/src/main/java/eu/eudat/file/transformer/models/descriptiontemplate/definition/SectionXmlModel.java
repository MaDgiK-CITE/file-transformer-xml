package eu.eudat.file.transformer.models.descriptiontemplate.definition;


import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import java.util.List;

@XmlType(namespace = "description-template")
public class SectionXmlModel {

	private String id;
	private Integer ordinal;
	private Boolean defaultVisibility;
	private Boolean multiplicity;
	private String numbering; //TODO maybe remove
	private String title;
	private String description;
	private String extendedDescription; //TODO maybe remove
	private List<SectionXmlModel> sections;
	private List<FieldSetXmlModel> fieldSets;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public Boolean getDefaultVisibility() {
		return defaultVisibility;
	}

	public void setDefaultVisibility(Boolean defaultVisibility) {
		this.defaultVisibility = defaultVisibility;
	}

	public Boolean getMultiplicity() {
		return multiplicity;
	}

	public void setMultiplicity(Boolean multiplicity) {
		this.multiplicity = multiplicity;
	}

	public String getNumbering() {
		return numbering;
	}

	public void setNumbering(String numbering) {
		this.numbering = numbering;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExtendedDescription() {
		return extendedDescription;
	}

	public void setExtendedDescription(String extendedDescription) {
		this.extendedDescription = extendedDescription;
	}

	public List<SectionXmlModel> getSections() {
		return sections;
	}

	public void setSections(List<SectionXmlModel> sectionXmlModels) {
		this.sections = sectionXmlModels;
	}

	public List<FieldSetXmlModel> getFieldSets() {
		return fieldSets;
	}

	public void setFieldSets(List<FieldSetXmlModel> fieldSetXmlModels) {
		this.fieldSets = fieldSetXmlModels;
	}
}


