package eu.eudat.file.transformer.xml;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.eudat.file.transformer.enums.FieldType;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.*;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata.ExternalDatasetDataXmlModel;
import org.springframework.core.env.Environment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ExportXmlBuilder {

    public File build(DefinitionXmlModel pagedDatasetProfile, UUID datasetProfileId, Environment environment) throws IOException {

        File xmlFile = new File(environment.getProperty("temp.temp") + UUID.randomUUID() + ".xml");
        BufferedWriter writer = new BufferedWriter(new FileWriter(xmlFile, true));
        Document xmlDoc = XmlBuilder.getDocument();
        Element root = xmlDoc.createElement("root");
        Element datasetProfile = xmlDoc.createElement("datasetProfileId");
        datasetProfile.setTextContent(datasetProfileId.toString());
        root.appendChild(datasetProfile);
        root.appendChild(createPages(pagedDatasetProfile.getPages(), xmlDoc));
        xmlDoc.appendChild(root);
        String xml = XmlBuilder.generateXml(xmlDoc);
        writer.write(xml);
        writer.close();
        return xmlFile;
    }

    public Element createPages(List<PageXmlModel> pagesModel, Document element) {
        Element pages = element.createElement("pages");
        pagesModel.forEach(item -> {
            Element page = element.createElement("page");
            page.appendChild(createSections(item.getSections(), element));
            pages.appendChild(page);
        });
        return pages;
    }

    private Element createSections(List<SectionXmlModel> sections, Document element) {
        Element elementSections = element.createElement("sections");
        sections.forEach(section -> {
            Element elementSection = element.createElement("section");
            /*if (visibilityRuleService.isElementVisible(section.getId()))*/ {
                elementSection.appendChild(createSections(section.getSections(), element));
                elementSection.appendChild(createCompositeFields(section.getFieldSets(), element));
                elementSections.appendChild(elementSection);
            }
        });
        return elementSections;
    }

    private Element createCompositeFields(List<FieldSetXmlModel> compositeFields, Document element) {
        Element elementComposites = element.createElement("composite-fields");
        compositeFields.forEach(compositeField -> {
            /*if (visibilityRuleService.isElementVisible(compositeField.getId()) && hasVisibleFields(compositeField, visibilityRuleService))*/ {
                Element composite = element.createElement("composite-field");
                composite.setAttribute("id", compositeField.getId());
                if (compositeField.getTitle() != null && !compositeField.getTitle().isEmpty()) {
                    Element title = element.createElement("title");
                    title.setTextContent(compositeField.getTitle());
                    composite.appendChild(title);
                }
                if (compositeField.getDescription() != null && !compositeField.getDescription().isEmpty()) {
                    Element title = element.createElement("description");
                    title.setTextContent(compositeField.getDescription());
                    composite.appendChild(title);
                }
                composite.appendChild(createFields(compositeField.getFields(), element));
                if(compositeField.getHasCommentField()){
                    Element comment = element.createElement("comment");
                    comment.setTextContent(compositeField.getExtendedDescription());
                    composite.appendChild(comment);
                }
                elementComposites.appendChild(composite);

            }
        });
        return elementComposites;
    }

    private Element createFields(List<FieldXmlModel> fields, Document element) {
        Element elementFields = element.createElement("fields");
        fields.forEach(field -> {
            /*if (visibilityRuleService.isElementVisible(field.getId()))*/ {
                Element elementField = element.createElement("field");
                elementField.setAttribute("id", field.getId());
                if (field.getData() != null && field.getData().getFieldType().equals(FieldType.EXTERNAL_DATASETS)) {
                    elementField.setAttribute("type", ((ExternalDatasetDataXmlModel)field.getData()).getType().getValue());
                }
                if (field.getData() != null && field.getData().getValue() != null) {
                    Element valueField = element.createElement("value");
                    ObjectMapper mapper = new ObjectMapper();
                    try {

                        List<Map<String, Object>> jsonArray = mapper.readValue(field.getData().getValue(), new TypeReference<>() {});
//                        JSONArray jsonArray = new JSONArray(field.getValue().toString());
                        StringBuilder sb = new StringBuilder();
                        boolean firstTime = true;
                        for (Map<String, Object> jsonElement: jsonArray) {
                            if (!firstTime) {
                                sb.append(", ");
                            }
                            sb.append(jsonElement.get("label") != null ? jsonElement.get("label") : jsonElement.get("name"));
                            firstTime = false;

                        }
                        /*for (int i = 0; i < jsonArray.length(); i++) {
                            sb.append(jsonArray.getJSONObject(i).get("label").toString());
                            if (i != jsonArray.length() - 1) sb.append(", ");
                        }*/
                        valueField.setTextContent(sb.toString());
                    } catch (IOException ex) {
                        try {
                            Map<String, Object> jsonElement = mapper.readValue(field.getData().getValue(), new TypeReference<>() {});
                            valueField.setTextContent((jsonElement.get("label") != null ? jsonElement.get("label").toString() : jsonElement.get("name") != null ? jsonElement.get("name").toString() : ""));
                        } catch (IOException e) {
                            try {
                                valueField.setTextContent(DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault()).format(Instant.parse(field.getData().getValue())));
                            } catch (Exception exc) {
                                valueField.setTextContent(field.getData().getValue());
                            }
                        }
                    }
                    elementField.appendChild(valueField);
                }
                elementFields.appendChild(elementField);
            }
        });
        return elementFields;
    }
}
