package eu.eudat.file.transformer.models.dmp;

import eu.eudat.file.transformer.enums.DmpUserRole;
import eu.eudat.file.transformer.models.user.UserXmlModel;

import java.util.UUID;

public class DmpUserXmlModel {

    private UUID id;
    private UserXmlModel user;
    private DmpUserRole role;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UserXmlModel getUser() {
        return user;
    }

    public void setUser(UserXmlModel user) {
        this.user = user;
    }

    public DmpUserRole getRole() {
        return role;
    }

    public void setRole(DmpUserRole role) {
        this.role = role;
    }
}
