package eu.eudat.file.transformer.models.dmpblueprint.definition;

import java.util.UUID;

public class DescriptionTemplateXmlModel {

	private UUID id;
	private UUID descriptionTemplateId;
	private String label;
	private Integer minMultiplicity;
	private Integer maxMultiplicity;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getDescriptionTemplateId() {
		return descriptionTemplateId;
	}

	public void setDescriptionTemplateId(UUID descriptionTemplateId) {
		this.descriptionTemplateId = descriptionTemplateId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getMinMultiplicity() {
		return minMultiplicity;
	}

	public void setMinMultiplicity(Integer minMultiplicity) {
		this.minMultiplicity = minMultiplicity;
	}

	public Integer getMaxMultiplicity() {
		return maxMultiplicity;
	}

	public void setMaxMultiplicity(Integer maxMultiplicity) {
		this.maxMultiplicity = maxMultiplicity;
	}
}
