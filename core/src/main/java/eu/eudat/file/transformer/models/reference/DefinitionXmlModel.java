package eu.eudat.file.transformer.models.reference;

import java.util.List;

public class DefinitionXmlModel {
	private List<FieldXmlModel> fields;

	public List<FieldXmlModel> getFields() {
		return fields;
	}

	public void setFields(List<FieldXmlModel> fields) {
		this.fields = fields;
	}
	
}
