package eu.eudat.file.transformer.models.descriptiontemplate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.DefinitionXmlModel;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescriptionTemplateXmlModel {
    private UUID id;
    private String label;
    private String description;
    private Short version;
    private String language;
    private DescriptionTemplateTypeFileTransformerModel type;
    private DefinitionXmlModel definition;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public DescriptionTemplateTypeFileTransformerModel getType() {
        return type;
    }

    public void setType(DescriptionTemplateTypeFileTransformerModel type) {
        this.type = type;
    }

    public DefinitionXmlModel getDefinition() {
        return definition;
    }

    public void setDefinition(DefinitionXmlModel definition) {
        this.definition = definition;
    }
}
