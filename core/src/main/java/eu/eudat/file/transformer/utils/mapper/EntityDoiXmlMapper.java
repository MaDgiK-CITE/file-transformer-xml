package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.entitydoi.EntityDoiFileTransformerModel;
import eu.eudat.file.transformer.models.entitydoi.EntityDoiXmlModel;

public class EntityDoiXmlMapper {

    public static EntityDoiXmlModel toXml(EntityDoiFileTransformerModel fileModel) {
        EntityDoiXmlModel xmlModel = new EntityDoiXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setDoi(fileModel.getDoi());
        xmlModel.setRepositoryId(fileModel.getRepositoryId());
        return xmlModel;
    }
}
