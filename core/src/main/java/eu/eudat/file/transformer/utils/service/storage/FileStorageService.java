package eu.eudat.file.transformer.utils.service.storage;

import eu.eudat.file.transformer.configuration.FileStorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.*;
import java.util.UUID;

@Service
public class FileStorageService {
    private final static Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    private final FileStorageProperties properties;

    @Autowired
    public FileStorageService(FileStorageProperties properties) {
        this.properties = properties;
    }

    public String storeFile(byte[] data) {
        try {
            String fileName = UUID.randomUUID().toString();
            Path storagePath = Paths.get(properties.getTransientPath() + "/" + fileName);
            Files.write(storagePath, data, StandardOpenOption.CREATE_NEW);
            return fileName;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public byte[] readFile(String fileRef) {
        try (FileInputStream inputStream = new FileInputStream(properties.getTransientPath() + "/" + fileRef)) {
            return inputStream.readAllBytes();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return new byte[1];
    }
}
