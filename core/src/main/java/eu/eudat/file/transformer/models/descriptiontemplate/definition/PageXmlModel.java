package eu.eudat.file.transformer.models.descriptiontemplate.definition;


import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement(name ="page")
public class PageXmlModel {
	private String id;
	private Integer ordinal;
	private String title;
	private List<SectionXmlModel> sections;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<SectionXmlModel> getSections() {
		return sections;
	}

	public void setSections(List<SectionXmlModel> sectionXmlModels) {
		this.sections = sectionXmlModels;
	}
}


