package eu.eudat.file.transformer.models.description;

import eu.eudat.file.transformer.enums.DescriptionStatus;
import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateXmlModel;
import eu.eudat.file.transformer.models.dmp.DmpXmlModel;
import eu.eudat.file.transformer.models.user.UserXmlModel;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
@XmlRootElement(name = "description")
@XmlAccessorType(XmlAccessType.FIELD)
public class DescriptionXmlModel {

    @XmlAttribute
    private UUID id;
    private String label;
    private String description;
    private UserXmlModel createdBy;
    private Instant createdAt;
    private Instant updatedAt;
    private Instant finalizedAt;
    private List<DescriptionReferenceXmlModel> descriptionReferenceXmlModels;
    private List<DescriptionTagXmlModel> descriptionTagXmlModels;
    private DescriptionTemplateXmlModel descriptionTemplate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserXmlModel getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserXmlModel createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getFinalizedAt() {
        return finalizedAt;
    }

    public void setFinalizedAt(Instant finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public List<DescriptionReferenceXmlModel> getDescriptionReferenceFileTransformerModels() {
        return descriptionReferenceXmlModels;
    }

    public void setDescriptionReferenceFileTransformerModels(List<DescriptionReferenceXmlModel> descriptionReferenceXmlModels) {
        this.descriptionReferenceXmlModels = descriptionReferenceXmlModels;
    }

    public List<DescriptionTagXmlModel> getDescriptionTagFileTransformerModels() {
        return descriptionTagXmlModels;
    }

    public void setDescriptionTagFileTransformerModels(List<DescriptionTagXmlModel> descriptionTagXmlModels) {
        this.descriptionTagXmlModels = descriptionTagXmlModels;
    }

    public DescriptionTemplateXmlModel getDescriptionTemplate() {
        return descriptionTemplate;
    }

    public void setDescriptionTemplate(DescriptionTemplateXmlModel descriptionTemplate) {
        this.descriptionTemplate = descriptionTemplate;
    }
}
