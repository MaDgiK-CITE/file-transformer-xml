package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.dmp.DmpFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpXmlModel;

public class DMPXmlMapper {

    public static DmpXmlModel toXml(DmpFileTransformerModel fileModel) {
        DmpXmlModel xmlModel = new DmpXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setDescription(fileModel.getDescription());
        xmlModel.setBlueprint(DmpBlueprintXmlMapper.toXml(fileModel.getBlueprint()));
        xmlModel.setCreator(UserXmlMapper.toXml(fileModel.getCreator()));
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setDescriptions(fileModel.getDescriptions().stream().map(DescriptionXmlMapper::toXml).toList());
        xmlModel.setAccessType(fileModel.getAccessType());
        xmlModel.setCreatedAt(fileModel.getCreatedAt());
        xmlModel.setDmpUsers(fileModel.getDmpUsers().stream().map(DmpUserXmlMapper::toXml).toList());
        xmlModel.setLanguage(fileModel.getLanguage());
        xmlModel.setVersion(fileModel.getVersion());
        if (fileModel.getEntityDois() != null) {
            xmlModel.setEntityDois(fileModel.getEntityDois().stream().map(EntityDoiXmlMapper::toXml).toList());
        }
        xmlModel.setProperties(fileModel.getProperties());
        xmlModel.setPublicAfter(fileModel.getPublicAfter());
        xmlModel.setUpdatedAt(fileModel.getUpdatedAt());
        xmlModel.setPublishedAt(fileModel.getPublishedAt());
        xmlModel.setDmpReferences(fileModel.getDmpReferences().stream().map(DmpReferenceXmlMapper::toXml).toList());
        return xmlModel;
    }
}
