package eu.eudat.file.transformer.utils.mapper.descriptiontemplate;

import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateFileTransformerModel;
import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.*;
import eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata.*;

public class DescriptionTemplateXmlMapper {

    public static DescriptionTemplateXmlModel toXml(DescriptionTemplateFileTransformerModel fileModel) {
        DescriptionTemplateXmlModel xmlModel = new DescriptionTemplateXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setDescription(fileModel.getDescription());
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setType(fileModel.getType());
        xmlModel.setLanguage(fileModel.getLanguage());
        xmlModel.setVersion(fileModel.getVersion());
        xmlModel.setDefinition(toDefinitionXml(fileModel.getDefinition()));

        return xmlModel;
    }

    private static DefinitionXmlModel toDefinitionXml(DefinitionFileTransformerModel fileModel) {
        DefinitionXmlModel xmlModel = new DefinitionXmlModel();

        xmlModel.setPages(fileModel.getPages().stream().map(DescriptionTemplateXmlMapper::toPageXml).toList());

        return xmlModel;
    }

    private static PageXmlModel toPageXml(PageFileTransformerModel fileModel) {
        PageXmlModel xmlModel = new PageXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setOrdinal(fileModel.getOrdinal());
        xmlModel.setTitle(fileModel.getTitle());
        xmlModel.setSections(fileModel.getSections().stream().map(DescriptionTemplateXmlMapper::toSectionXml).toList());

        return xmlModel;
    }

    private static SectionXmlModel toSectionXml(SectionFileTransformerModel fileModel) {
        SectionXmlModel xmlModel = new SectionXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setDescription(fileModel.getDescription());
        if (fileModel.getSections() != null) {
            xmlModel.setSections(fileModel.getSections().stream().map(DescriptionTemplateXmlMapper::toSectionXml).toList());
        }
        if (fileModel.getFieldSets() != null) {
            xmlModel.setFieldSets(fileModel.getFieldSets().stream().map(DescriptionTemplateXmlMapper::toFieldSetXml).toList());xmlModel.setExtendedDescription(fileModel.getExtendedDescription());
        }
        xmlModel.setTitle(fileModel.getTitle());
        xmlModel.setOrdinal(fileModel.getOrdinal());
        xmlModel.setDefaultVisibility(fileModel.getDefaultVisibility());
        return xmlModel;
    }

    private static FieldSetXmlModel toFieldSetXml(FieldSetFileTransformerModel fileModel) {
        FieldSetXmlModel xmlModel = new FieldSetXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setTitle(fileModel.getTitle());
        xmlModel.setDescription(fileModel.getDescription());
        xmlModel.setExtendedDescription(fileModel.getExtendedDescription());
        xmlModel.setOrdinal(fileModel.getOrdinal());
        xmlModel.setAdditionalInformation(fileModel.getAdditionalInformation());
        xmlModel.setHasCommentField(fileModel.getHasCommentField());
        xmlModel.setNumbering(fileModel.getNumbering());
        xmlModel.setFields(fileModel.getFields().stream().map(DescriptionTemplateXmlMapper::toFieldXml).toList());
        if (fileModel.getMultiplicity() != null) {
            xmlModel.setMultiplicity(toMultiplicityXml(fileModel.getMultiplicity()));
        }
        return xmlModel;
    }

    private static FieldXmlModel toFieldXml(FieldFileTransformerModel fileModel) {
        FieldXmlModel xmlModel = new FieldXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setDefaultValue(fileModel.getDefaultValue());
        xmlModel.setNumbering(fileModel.getNumbering());
        xmlModel.setOrdinal(fileModel.getOrdinal());
        xmlModel.setSchematics(fileModel.getSchematics());
        xmlModel.setValidations(fileModel.getValidations());
        xmlModel.setIncludeInExport(fileModel.getIncludeInExport());
        if (fileModel.getData() != null) {
            xmlModel.setData(toDataXml(fileModel.getData()));
        }
        if (fileModel.getVisibilityRules() != null) {
            xmlModel.setVisibilityRules(fileModel.getVisibilityRules().stream().map(DescriptionTemplateXmlMapper::toRuleXml).toList());
        }

        return xmlModel;
    }

    private static BaseFieldDataXmlModel toDataXml(BaseFieldDataFileTransformerModel fileModel) {
        BaseFieldDataXmlModel xmlModel = switch (fileModel.getFieldType()) {
            case ORGANIZATIONS,
                    LICENSES,
                    PUBLICATIONS,
                    BOOLEAN_DECISION,
                    INTERNAL_DMP_ENTRIES_DATASETS,
                    INTERNAL_DMP_ENTRIES_DMPS,
                    REGISTRIES,
                    INTERNAL_DMP_ENTRIES_RESEARCHERS,
                    RESEARCHERS,
                    SERVICES,
                    TAXONOMIES,
                    DATA_REPOSITORIES,
                    JOURNAL_REPOSITORIES -> new LabelAndMultiplicityDataXmlModel();
            case EXTERNAL_SELECT -> new ExternalSelectDataXmlModel();
            case CHECK_BOX,
                    DATE_PICKER,
                    FREE_TEXT,
                    RICH_TEXT_AREA,
                    TAGS,
                    TEXT_AREA,
                    VALIDATION,
                    DATASET_IDENTIFIER,
                    CURRENCY -> new LabelDataXmlModel();
            case UPLOAD -> new UploadDataXmlModel();
            case SELECT -> new SelectDataXmlModel();
            case RADIO_BOX -> new RadioBoxDataXmlModel();
            case EXTERNAL_DATASETS -> new ExternalDatasetDataXmlModel();
            default -> null;
        };

        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setValue(fileModel.getValue());
        xmlModel.setFieldType(fileModel.getFieldType());
        switch (xmlModel) {
            case ExternalDatasetDataXmlModel sm -> sm.setType(((ExternalDatasetDataFileTransformerModel)fileModel).getType());
            case ExternalSelectDataXmlModel sm -> {
                ExternalSelectDataFileTransformerModel fm = ((ExternalSelectDataFileTransformerModel) fileModel);
                sm.setMultipleSelect(fm.getMultipleSelect());
                sm.setSources(fm.getSources().stream().map(FieldDataXmlMapper::toAutocompleteXml).toList());
            }
            case SelectDataXmlModel sm -> {
                SelectDataFileTransformerModel fm = (SelectDataFileTransformerModel) fileModel;
                sm.setMultipleSelect(fm.getMultipleSelect());
                sm.setOptions(fm.getOptions().stream().map(FieldDataXmlMapper::toSelectOptionXml).toList());
            }
            case LabelAndMultiplicityDataXmlModel sm -> sm.setMultipleSelect(((LabelAndMultiplicityDataFileTransformerModel)fileModel).getMultipleSelect());
            case UploadDataXmlModel sm -> {
                UploadDataFileTransformerModel fm = ((UploadDataFileTransformerModel) fileModel);
                sm.setMaxFileSizeInMB(fm.getMaxFileSizeInMB());
                sm.setTypes(fm.getTypes().stream().map(FieldDataXmlMapper::toUploadXml).toList());
            }
            case RadioBoxDataXmlModel sm -> {
                RadioBoxDataFileTransformerModel fm = (RadioBoxDataFileTransformerModel) fileModel;
                sm.setOptions(fm.getOptions().stream().map(FieldDataXmlMapper::toRadioBoxXml).toList());
            }
            default -> {}
        }


        return xmlModel;
    }

    private static RuleXmlModel toRuleXml(RuleFileTransformerModel fileModel) {
        RuleXmlModel xmlModel = new RuleXmlModel();
        xmlModel.setTarget(fileModel.getTarget());
        xmlModel.setValue(fileModel.getValue());
        return xmlModel;
    }

    private static MultiplicityXmlModel toMultiplicityXml(MultiplicityFileTransformerModel fileModel) {
        MultiplicityXmlModel xmlModel = new MultiplicityXmlModel();

        xmlModel.setMax(fileModel.getMax());
        xmlModel.setMin(fileModel.getMin());
        xmlModel.setPlaceholder(fileModel.getPlaceholder());
        xmlModel.setTableView(fileModel.getTableView());

        return xmlModel;
    }

 }
