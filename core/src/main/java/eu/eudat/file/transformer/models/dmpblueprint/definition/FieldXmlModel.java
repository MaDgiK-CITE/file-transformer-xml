package eu.eudat.file.transformer.models.dmpblueprint.definition;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import eu.eudat.file.transformer.enums.DmpBlueprintFieldCategory;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

import java.util.UUID;

@XmlTransient
@XmlSeeAlso({ExtraFieldXmlModel.class, SystemFieldXmlModel.class})
@XmlType(namespace = "dmp-blueprint")
/*@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "category",
		visible = true)
@JsonSubTypes({
		@JsonSubTypes.Type(value = SystemFieldXmlModel.class, name = "0"),
		@JsonSubTypes.Type(value = ExtraFieldXmlModel.class, name = "1")
})*/
public abstract class FieldXmlModel {

	private UUID id;
	private DmpBlueprintFieldCategory category;
	private String label;
	private String placeholder;
	private String description;
	private Integer ordinal;
	private Boolean required;


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public DmpBlueprintFieldCategory getCategory() {
		return category;
	}

	public void setCategory(DmpBlueprintFieldCategory category) {
		this.category = category;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public Boolean isRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}
}
