package eu.eudat.file.transformer.models.dmpblueprint.definition;

import jakarta.xml.bind.annotation.XmlType;

import java.util.List;

@XmlType(namespace = "dmp-blueprint")
public class DefinitionXmlModel {

	private List<SectionXmlModel> sectionXmlModels;

	public List<SectionXmlModel> getSections() {
		return sectionXmlModels;
	}

	public void setSections(List<SectionXmlModel> sectionXmlModels) {
		this.sectionXmlModels = sectionXmlModels;
	}
}
