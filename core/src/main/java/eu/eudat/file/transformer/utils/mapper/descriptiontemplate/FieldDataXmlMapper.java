package eu.eudat.file.transformer.utils.mapper.descriptiontemplate;

import eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata.*;

public class FieldDataXmlMapper {

    public static ExternalSelectDataXmlModel.ExternalSelectSourceXmlModel toAutocompleteXml(ExternalSelectDataFileTransformerModel.ExternalSelectSourceFileTransformerModel fileModel) {
        ExternalSelectDataXmlModel.ExternalSelectSourceXmlModel xmlModel = new ExternalSelectDataXmlModel.ExternalSelectSourceXmlModel();

        xmlModel.setUrl(fileModel.getUrl());
        xmlModel.setMethod(fileModel.getMethod());
        xmlModel.setOptionsRoot(fileModel.getOptionsRoot());
        xmlModel.setHasAuth(fileModel.getHasAuth());
        if (fileModel.getAuth() != null) {
            xmlModel.setAuth(toAuthXml(fileModel.getAuth()));
        }
        xmlModel.setSourceBinding(toExternalSelectSourceBindingXml(fileModel.getSourceBinding()));

        return xmlModel;
    }

    private static ExternalSelectDataXmlModel.ExternalSelectAuthDataXmlModel toAuthXml(ExternalSelectDataFileTransformerModel.ExternalSelectAuthDataFileTransformerModel fileModel) {
        ExternalSelectDataXmlModel.ExternalSelectAuthDataXmlModel xmlModel = new ExternalSelectDataXmlModel.ExternalSelectAuthDataXmlModel();
        xmlModel.setUrl(fileModel.getUrl());
        xmlModel.setType(fileModel.getType());
        xmlModel.setBody(fileModel.getBody());
        xmlModel.setMethod(fileModel.getMethod());
        xmlModel.setPath(fileModel.getPath());
        return xmlModel;
    }

    public static ExternalSelectDataXmlModel.ExternalSelectSourceBindingXmlModel toExternalSelectSourceBindingXml(ExternalSelectDataFileTransformerModel.ExternalSelectSourceBindingFileTransformerModel fileModel) {
        ExternalSelectDataXmlModel.ExternalSelectSourceBindingXmlModel xmlModel = new ExternalSelectDataXmlModel.ExternalSelectSourceBindingXmlModel();

        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setValue(fileModel.getValue());
        xmlModel.setSource(fileModel.getSource());

        return xmlModel;
    }

    public static SelectDataXmlModel.OptionXmlModel toSelectOptionXml(SelectDataFileTransformerModel.OptionFileTransformerModel fileModel) {
        SelectDataXmlModel.OptionXmlModel xmlModel = new SelectDataXmlModel.OptionXmlModel();

        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setValue(fileModel.getValue());

        return xmlModel;
    }

    public static UploadDataXmlModel.UploadDataOptionXmlModel toUploadXml(UploadDataFileTransformerModel.UploadDataOptionFileTransformerModel fileModel) {
        UploadDataXmlModel.UploadDataOptionXmlModel xmlModel = new UploadDataXmlModel.UploadDataOptionXmlModel();
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setValue(fileModel.getValue());
        return xmlModel;
    }

    public static RadioBoxDataXmlModel.RadioBoxDataOptionXmlModel toRadioBoxXml(RadioBoxDataFileTransformerModel.RadioBoxDataOptionFileTransformerModel fileModel) {
        RadioBoxDataXmlModel.RadioBoxDataOptionXmlModel xmlModel = new RadioBoxDataXmlModel.RadioBoxDataOptionXmlModel();
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setValue(fileModel.getValue());
        return xmlModel;
    }
}
