package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.dmp.DmpUserFileTransformerModel;
import eu.eudat.file.transformer.models.dmp.DmpUserXmlModel;

public class DmpUserXmlMapper {

    public static DmpUserXmlModel toXml(DmpUserFileTransformerModel fileModel) {
        DmpUserXmlModel xmlModel = new DmpUserXmlModel();
        xmlModel.setId(fileModel.getId());
        xmlModel.setRole(fileModel.getRole());
        xmlModel.setUser(UserXmlMapper.toXml(fileModel.getUser()));
        return xmlModel;
    }
}
