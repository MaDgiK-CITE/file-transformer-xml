package eu.eudat.file.transformer.models.descriptiontemplate.definition;

import java.util.List;

public class FieldSetXmlModel {

	private String id;
	private Integer ordinal;
	private String numbering;
	private String title;
	private String description;
	private String extendedDescription;
	private String additionalInformation;
	private MultiplicityXmlModel multiplicity;
	private Boolean hasCommentField;
	private List<FieldXmlModel> fields;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public String getNumbering() {
		return numbering;
	}

	public void setNumbering(String numbering) {
		this.numbering = numbering;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExtendedDescription() {
		return extendedDescription;
	}

	public void setExtendedDescription(String extendedDescription) {
		this.extendedDescription = extendedDescription;
	}

	public String getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public MultiplicityXmlModel getMultiplicity() {
		return multiplicity;
	}

	public void setMultiplicity(MultiplicityXmlModel multiplicityXmlModel) {
		this.multiplicity = multiplicityXmlModel;
	}

	public Boolean getHasCommentField() {
		return hasCommentField;
	}

	public void setHasCommentField(Boolean hasCommentField) {
		this.hasCommentField = hasCommentField;
	}

	public List<FieldXmlModel> getFields() {
		return fields;
	}

	public void setFields(List<FieldXmlModel> fieldXmlModels) {
		this.fields = fieldXmlModels;
	}
}
