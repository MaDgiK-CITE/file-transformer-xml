package eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata;

import eu.eudat.file.transformer.enums.FieldType;
import jakarta.xml.bind.annotation.*;

import java.util.List;


@XmlRootElement(name = FieldType.Names.ExternalSelect)
public class ExternalSelectDataXmlModel extends LabelAndMultiplicityDataXmlModel {
    public static class ExternalSelectAuthDataXmlModel {
        private String url;
        private String method;
        private String body;
        private String path;
        private String type;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class ExternalSelectSourceBindingXmlModel {
        private String label;
        private String value;
        private String source;

        public String getLabel() {
            return label;
        }
        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }

        public String getSource() {
            return source;
        }
        public void setSource(String source) {
            this.source = source;
        }
    }

    public static class ExternalSelectSourceXmlModel {
        private String url;
        private String method;
        private String optionsRoot;
        private ExternalSelectSourceBindingXmlModel sourceBinding;
        private Boolean hasAuth;
        private ExternalSelectAuthDataXmlModel auth;

        public String getOptionsRoot() {
            return optionsRoot;
        }
        public void setOptionsRoot(String optionsRoot) {
            this.optionsRoot = optionsRoot;
        }

        public String getUrl() {
            return url;
        }
        public void setUrl(String url) {
            this.url = url;
        }

        public Boolean getHasAuth() {
            return hasAuth;
        }

        public void setHasAuth(Boolean hasAuth) {
            this.hasAuth = hasAuth;
        }

        public ExternalSelectAuthDataXmlModel getAuth() {
            return auth;
        }

        public void setAuth(ExternalSelectAuthDataXmlModel auth) {
            this.auth = auth;
        }

        public ExternalSelectSourceBindingXmlModel getSourceBinding() {
            return sourceBinding;
        }
        public void setSourceBinding(ExternalSelectSourceBindingXmlModel sourceBinding) {
            this.sourceBinding = sourceBinding;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }
    }

    private List<ExternalSelectSourceXmlModel> sources;

    public List<ExternalSelectSourceXmlModel> getSources() {
        return sources;
    }

    public void setSources(List<ExternalSelectSourceXmlModel> externalSelectSourceXmlModelList) {
        this.sources = externalSelectSourceXmlModelList;
    }
}


