package eu.eudat.file.transformer.models.description;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.eudat.file.transformer.models.reference.ReferenceXmlModel;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DescriptionReferenceXmlModel {

    private UUID id;

    private ReferenceXmlModel reference;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ReferenceXmlModel getReference() {
        return reference;
    }

    public void setReference(ReferenceXmlModel reference) {
        this.reference = reference;
    }

}
