package eu.eudat.file.transformer.models.dmpblueprint.definition;


import jakarta.xml.bind.annotation.XmlType;

import java.util.List;
import java.util.UUID;

@XmlType(namespace = "dmp-blueprint")
public class SectionXmlModel {
	private UUID id;
	private String description;
	private String label;
	private Integer ordinal;
	private Boolean hasTemplates;
	private List<FieldXmlModel> fieldXmlModels;
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public Boolean getHasTemplates() {
		return hasTemplates;
	}

	public void setHasTemplates(Boolean hasTemplates) {
		this.hasTemplates = hasTemplates;
	}

	public List<FieldXmlModel> getFields() {
		return fieldXmlModels;
	}

	public void setFields(List<FieldXmlModel> fieldXmlModels) {
		this.fieldXmlModels = fieldXmlModels;
	}
}


