package eu.eudat.file.transformer.models.dmp;

import eu.eudat.file.transformer.enums.DmpAccessType;
import eu.eudat.file.transformer.enums.DmpStatus;
import eu.eudat.file.transformer.enums.DmpVersionStatus;
import eu.eudat.file.transformer.models.description.DescriptionXmlModel;
import eu.eudat.file.transformer.models.dmpblueprint.DmpBlueprintXmlModel;
import eu.eudat.file.transformer.models.entitydoi.EntityDoiXmlModel;
import eu.eudat.file.transformer.models.user.UserXmlModel;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@XmlRootElement(name = "DMP")
@XmlAccessorType(XmlAccessType.FIELD)
public class DmpXmlModel {

    @XmlAttribute
    private UUID id;
    private String label;
    private String description;
    @XmlAttribute
    private Short version;
    private String properties;
    private Instant createdAt;
    private Instant updatedAt;
    private Instant finalizedAt;
    private Instant publishedAt;
    private UserXmlModel creator;
    private DmpAccessType accessType;
    private DmpBlueprintXmlModel blueprint;
    private String language;
    private Instant publicAfter;
    private List<DmpReferenceXmlModel> dmpReferences;
    private List<DmpUserXmlModel> dmpUsers;
    private List<DescriptionXmlModel> descriptions;
    private List<EntityDoiXmlModel> entityDois;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getFinalizedAt() {
        return finalizedAt;
    }

    public void setFinalizedAt(Instant finalizedAt) {
        this.finalizedAt = finalizedAt;
    }

    public Instant getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Instant publishedAt) {
        this.publishedAt = publishedAt;
    }

    public UserXmlModel getCreator() {
        return creator;
    }

    public void setCreator(UserXmlModel creator) {
        this.creator = creator;
    }

    public DmpAccessType getAccessType() {
        return accessType;
    }

    public void setAccessType(DmpAccessType accessType) {
        this.accessType = accessType;
    }

    public DmpBlueprintXmlModel getBlueprint() {
        return blueprint;
    }

    public void setBlueprint(DmpBlueprintXmlModel blueprint) {
        this.blueprint = blueprint;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Instant getPublicAfter() {
        return publicAfter;
    }

    public void setPublicAfter(Instant publicAfter) {
        this.publicAfter = publicAfter;
    }

    public List<DmpReferenceXmlModel> getDmpReferences() {
        return dmpReferences;
    }

    public void setDmpReferences(List<DmpReferenceXmlModel> dmpReferences) {
        this.dmpReferences = dmpReferences;
    }

    public List<DmpUserXmlModel> getDmpUsers() {
        return dmpUsers;
    }

    public void setDmpUsers(List<DmpUserXmlModel> dmpUsers) {
        this.dmpUsers = dmpUsers;
    }

    public List<DescriptionXmlModel> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DescriptionXmlModel> descriptions) {
        this.descriptions = descriptions;
    }

    public List<EntityDoiXmlModel> getEntityDois() {
        return entityDois;
    }

    public void setEntityDois(List<EntityDoiXmlModel> entityDois) {
        this.entityDois = entityDois;
    }
}
