package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.description.DescriptionFileTransformerModel;
import eu.eudat.file.transformer.models.description.DescriptionXmlModel;
import eu.eudat.file.transformer.models.descriptiontemplate.DescriptionTemplateXmlModel;
import eu.eudat.file.transformer.utils.mapper.descriptiontemplate.DescriptionTemplateXmlMapper;

public class DescriptionXmlMapper {

    public static DescriptionXmlModel toXml(DescriptionFileTransformerModel fileModel) {
        DescriptionXmlModel xmlModel = new DescriptionXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setLabel(fileModel.getLabel());
        xmlModel.setDescription(fileModel.getDescription());
        xmlModel.setCreatedAt(fileModel.getCreatedAt());
        xmlModel.setUpdatedAt(fileModel.getUpdatedAt());
        xmlModel.setFinalizedAt(fileModel.getFinalizedAt());
        xmlModel.setDescriptionTemplate(DescriptionTemplateXmlMapper.toXml(fileModel.getDescriptionTemplate()));
        xmlModel.setCreatedBy(UserXmlMapper.toXml(fileModel.getCreatedBy()));
        if (fileModel.getDescriptionReferenceFileTransformerModels() != null) {
            xmlModel.setDescriptionReferenceFileTransformerModels(fileModel.getDescriptionReferenceFileTransformerModels().stream().map(DescriptionReferenceXmlMapper::toXml).toList());
        }
        if (fileModel.getDescriptionTagFileTransformerModels() != null) {
            xmlModel.setDescriptionTagFileTransformerModels(fileModel.getDescriptionTagFileTransformerModels().stream().map(DescriptionTagXmlMapper::toXml).toList());
        }
        return xmlModel;
    }
}
