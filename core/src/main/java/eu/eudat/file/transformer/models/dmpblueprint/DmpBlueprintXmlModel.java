package eu.eudat.file.transformer.models.dmpblueprint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.eudat.file.transformer.models.dmpblueprint.definition.DefinitionXmlModel;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.UUID;

@XmlRootElement(name = "blueprint")
public class DmpBlueprintXmlModel {

    private UUID id;
    private String label;
    private DefinitionXmlModel definitionXmlModel;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DefinitionXmlModel getDefinitionFileTransformerModel() {
        return definitionXmlModel;
    }

    public void setDefinitionFileTransformerModel(DefinitionXmlModel definitionXmlModel) {
        this.definitionXmlModel = definitionXmlModel;
    }
}



