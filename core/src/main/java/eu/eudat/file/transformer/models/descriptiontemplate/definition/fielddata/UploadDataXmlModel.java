package eu.eudat.file.transformer.models.descriptiontemplate.definition.fielddata;

import eu.eudat.file.transformer.enums.FieldType;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement(name = FieldType.Names.Upload)
public class UploadDataXmlModel extends BaseFieldDataXmlModel {
    public static class UploadDataOptionXmlModel {
        private String label;
        private String value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private List<UploadDataOptionXmlModel> types;

    private Integer maxFileSizeInMB;


    public List<UploadDataOptionXmlModel> getTypes() {
        return types;
    }

    public void setTypes(List<UploadDataOptionXmlModel> types) {
        this.types = types;
    }

    public Integer getMaxFileSizeInMB() {
        return maxFileSizeInMB;
    }

    public void setMaxFileSizeInMB(Integer maxFileSizeInMB) {
        this.maxFileSizeInMB = maxFileSizeInMB;
    }
}
