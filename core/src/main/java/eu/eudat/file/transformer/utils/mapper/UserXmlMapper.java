package eu.eudat.file.transformer.utils.mapper;

import eu.eudat.file.transformer.models.user.UserFileTransformerModel;
import eu.eudat.file.transformer.models.user.UserXmlModel;

public class UserXmlMapper {

    public static UserXmlModel toXml(UserFileTransformerModel fileModel) {
        UserXmlModel xmlModel = new UserXmlModel();

        xmlModel.setId(fileModel.getId());
        xmlModel.setName(fileModel.getName());

        return xmlModel;
    }
}
